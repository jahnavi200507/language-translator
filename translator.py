from tkinter import *
import googletrans
import textblob
import pyttsx3
from tkinter import ttk,messagebox
root=Tk()
root.title('My App')
root['bg']='black'
root.geometry("935x300+300+200")
root.iconbitmap("C:\\Users\janga\Desktop\project1.py")
def translate_it():
    #Delete any previous translations
    translated_text.delete(1.0, END)
    try:
       #Get Languages From Dictionary Keys
       #Get the From Language Key
       for key, value in languages.items():
           if(value == original_combo.get()):
               from_language_key = key

       #Get the To Language Key
       for key, value in languages.items():
           if(value == translated_combo.get()):
               to_language_key = key

       #Turn Original Text into a TextBlob
       words = textblob.TextBlob(original_text.get(1.0,END))
       
       #Translate Text
       words = words.translate(from_lang = from_language_key ,to = to_language_key)

       #Initialize speech engine
       engine = pyttsx3.init()

       #play with voices
       voices = engine.getProperty("voices")
       #for voice in voices :
         #engine.setProperty('voice' , voice.id)
         #engine.say(words)

       #pass text to speech engine
       engine.say(words)

       #run the engine
       engine.runAndWait()

       #Output translated text to screen
       translated_text.insert(1.0, words)
    
    except Exception as e:
        messagebox.showerror("Translator",e)
def clear():
    #clear the text boxes
    original_text.delete(1.0,END)
    translated_text.delete(1.0,END)

#language_list=(1,2,3,4,5,6,7,8,9,0,11,12,13,14,15,16,16,1,1,1,1,1,1,1,1,1,1,1,1)

#Grab Language list from googletrans
languages=googletrans.LANGUAGES

language_list=list(languages.values())
print(language_list)
#text boxes
original_text=Text(root,font=("Helvetica",10),height=10,width=45)
original_text.grid(row=0,column=0,pady=20,padx=10)

translate_button=Button(root,text="TRANSLATE!",font=("Helvetica",24),command=translate_it)
translate_button.grid(row=0,column=1,padx=10)

translated_text=Text(root,font=("Helvetica",10),height=10,width=45)
translated_text.grid(row=0,column=2,pady=20,padx=10)

#combo boxes
original_combo=ttk.Combobox(root,font=("Helvetica",10),width=40,value=language_list)
original_combo.current(21)
original_combo.grid(row=1,column=0)

translated_combo=ttk.Combobox(root,font=("Helvetica",10),width=40,value=language_list)
translated_combo.current(26)
translated_combo.grid(row=1,column=2)
#clear button
clear_button=Button(root,text="clear",font=("Helvetica",15),command=clear,height=1,width=7)
clear_button.grid(row=2,column=1)
root.mainloop()